package pagefault.sort

import io.pagefault.sort.MergeSorter
import org.junit.jupiter.params.provider.Arguments
import java.util.stream.Stream

class MergeSorterTest : SorterTest() {
    override fun sortProvider(): Stream<Arguments> = Stream.of(Arguments.of(MergeSorter))
}
