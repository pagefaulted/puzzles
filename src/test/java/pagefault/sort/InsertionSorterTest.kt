package pagefault.sort

import io.pagefault.sort.InsertionSorter
import org.junit.jupiter.params.provider.Arguments
import java.util.stream.Stream

class InsertionSorterTest : SorterTest() {
    override fun sortProvider(): Stream<Arguments> = Stream.of(Arguments.of(InsertionSorter.adapted()))
}
