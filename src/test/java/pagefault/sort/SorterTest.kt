package pagefault.sort

import io.pagefault.sort.InPlaceSorter
import io.pagefault.sort.Sorter
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class SorterTest {
    abstract fun sortProvider(): Stream<Arguments>

    @ParameterizedTest
    @MethodSource("sortProvider")
    fun `sorting an already sorted list should be a nop`(sorter: Sorter) {
        val sortedList = listOf(1L, 2, 3, 4, 5)
        assertEquals(sortedList, sorter.sort(sortedList))
    }

    @ParameterizedTest
    @MethodSource("sortProvider")
    fun `sorting an empty list should be a nop`(sorter: Sorter) {
        val emptyList = listOf<Long>()
        assertEquals(emptyList, sorter.sort(emptyList))
    }

    @ParameterizedTest
    @MethodSource("sortProvider")
    fun `sorting a non-empty list should ehm sort it`(sorter: Sorter) {
        val unsortedList = listOf(5L, 3, 4, 6, 1)
        assertEquals(unsortedList.sorted(), sorter.sort(unsortedList))
    }
}

/**
 * Wraps this [InPlaceSorter] with a [Sorter], for compatibility with [SorterTest].
 */
fun InPlaceSorter.adapted(): Sorter = InPlaceSorterAdapter(this)

/**
 * Adapter class to be able to re-use the same test suite for both [Sorter] and [InPlaceSorter] tests.
 */
private class InPlaceSorterAdapter(private val inPlaceSorter: InPlaceSorter): Sorter {
    override fun sort(elements: List<Long>): List<Long> =
        elements
            .toMutableList()
            .also { inPlaceSorter.sort(it) }
}
