package pagefault.sort

import io.pagefault.sort.SelectionSorter
import org.junit.jupiter.params.provider.Arguments
import java.util.stream.Stream

class SelectionSorterTest : SorterTest() {
    override fun sortProvider(): Stream<Arguments> = Stream.of(Arguments.of(SelectionSorter.adapted()))
}
