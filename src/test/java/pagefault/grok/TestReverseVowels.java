package pagefault.grok;

import io.pagefault.grok.ReverseVowels;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestReverseVowels {

    @Test
    public void testReverseVowels() {
        Assertions.assertEquals("holle", new ReverseVowels().reverseVowels("hello"));
        assertEquals("leotcede", new ReverseVowels().reverseVowels("leetcode"));
        assertEquals("Aa", new ReverseVowels().reverseVowels("aA"));
        assertEquals("aA", new ReverseVowels().reverseVowels("Aa"));
        assertEquals("a.", new ReverseVowels().reverseVowels("a."));
        assertEquals("a", new ReverseVowels().reverseVowels("a"));
        assertEquals("", new ReverseVowels().reverseVowels(""));
        assertNull(new ReverseVowels().reverseVowels(null));
    }
}
