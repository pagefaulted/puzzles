package pagefault.search

import org.junit.jupiter.api.Test

class SearchTest {
    @Test
    fun `searching an empty list should return nothing`() {

    }

    @Test
    fun `searching for a non-existing element should return nothing`() {

    }

    @Test
    fun `searching for an existing element should return the index of the first occurrence`() {

    }
}
