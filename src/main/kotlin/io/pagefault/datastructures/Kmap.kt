package io.pagefault.datastructures

import java.util.LinkedList

interface Kmap<K, V> {
    /**
     * Get the value [V] associated with the [key], if one exists.
     *
     * @param key the key to get the associated value of.
     * @return the value associated with the key, if one exists.
     */
    fun get(key: K): V?

    /**
     * Put the [key] [value] pair in this map.
     *
     * @param key the key
     * @param value the value
     */
    fun put(key: K, value: V)
}

/**
 * Simple HashMap implementation.
 *
 * Not thread safe.
 */
class SimpleHashMap<K, V> : Kmap<K, V> {
    private companion object {
        const val INITIAL_BUCKETS_COUNT = 8 shl 2;
        const val LOAD_FACTOR = 0.75
    }

    private var size = 0L

    private var buckets = ArrayList<LinkedList<Entry<K, V>>>(INITIAL_BUCKETS_COUNT)

    override fun get(key: K): V? {
        TODO("Not yet implemented")
    }

    override fun put(key: K, value: V) {
        ++size
        if (shouldResize()) resize()
        val bucket = buckets.getOrElse(hash(key)) { LinkedList<Entry<K, V>>() }
        bucket
            .find { it.key == key }
            .let {
                if (it != null) {
                    it.value = value
                } else {
                    bucket.add(Entry(key, value))
                }
            }
    }

    private fun resize() {
        // Buckets count must be a power of 2.
        val newBuckets = ArrayList<LinkedList<Entry<K, V>>>(buckets.size shl 1)
        buckets.forEach { b ->
            b.forEach { e ->
                newBuckets
                    .getOrElse(hash(e.key)) { LinkedList<Entry<K, V>>() }
                    .add(e)
            }
        }

        buckets = newBuckets
    }

    /**
     * The bucket count is always a power of 2. The modulo operation can be performed with a bitwise and, which should
     * be cheaper than the modulo.
     */
    private fun hash(key: K): Int = key.hashCode() and buckets.size
    private fun shouldResize(): Boolean = size > (buckets.size * LOAD_FACTOR)

    private data class Entry<K,V>(val key: K, var value: V)

}

fun main() {
    println(-1 ushr Integer.numberOfLeadingZeros(64))
}
