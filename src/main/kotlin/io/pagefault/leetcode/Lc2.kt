package io.pagefault.leetcode

/*

You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order, and each of their nodes contains a single digit.
Add the two numbers and return the sum as a linked list.
You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 */

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
data class ListNode(var `val`: Int) {
    var next: ListNode? = null

    companion object Empty
}

class Solution {
    fun addTwoNumbers(l1: ListNode, l2: ListNode): ListNode {
        val result = (toIntString(l1)!!.toInt() + toIntString(l2)!!.toInt()).toString().reversed()
        val first = ListNode(result.first().toString().toInt())
        result.subSequence(1, result.length).fold(first) { listNode, char ->
            ListNode(char.toString().toInt()).also {
                listNode.next = it
            }
        }
        return first
    }

    private fun toIntString(ln: ListNode): String? =
        if (ln.next != null) {
            toIntString(ln.next!!) + ln.`val`.toString()
        } else {
            ln.`val`.toString()
        }
}

fun main() {
    val one = ListNode(1)
    val two = ListNode(6)
    val three = ListNode(3)
    one.next = two
    two.next = three

    val four = ListNode(4)
    val five = ListNode(5)
    val six = ListNode(6)
    four.next = five
    five.next = six

    println(Solution().addTwoNumbers(one, four))
}
