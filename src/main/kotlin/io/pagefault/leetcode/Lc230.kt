package io.pagefault.leetcode

import io.pagefault.leetcode.Lc230.Recursive

class TreeNode(val value: Int, val left: TreeNode? = null, val right: TreeNode? = null)

/**
 * Given the root of a binary search tree, and an integer k,
 * return the kth smallest value (1-indexed) of all the values of the nodes in the tree.
 */
class Lc230 {

    class Recursive {
        fun kthSmallest(root: TreeNode?, k: Int): Int = toOrderedArray(root)[k - 1]

        fun toOrderedArray(root: TreeNode?): Array<Int> =
            root?.let {
                toOrderedArray(root.left) + arrayOf(root.value) + toOrderedArray(root.right)
            } ?: emptyArray()
    }

    class Iterative {
        fun kthSmallest(root: TreeNode?, k: Int): Int = toOrderedArray(root)[k - 1]

        fun toOrderedArray(root: TreeNode?): Array<Int> {
            if (root == null) {
                return emptyArray()
            }

            val result = mutableListOf<Int>()
            val toVisit = mutableListOf<TreeNode>()
            pushLeftToLeaf(root, toVisit)

            while (toVisit.isNotEmpty()) {
                val leaf = toVisit.removeLast()
                result.add(leaf.value)
                if (leaf.right != null) {
                    pushLeftToLeaf(leaf.right, toVisit)
                }
            }
            return result.toTypedArray()
        }

        private fun pushLeftToLeaf(vertex: TreeNode, toVisit: MutableList<TreeNode>) {
            var v = vertex
            toVisit.add(v)
            while (v.left != null) {
                toVisit.add(v.left!!)
                v = v.left!!
            }
        }
    }
}

fun main() {
    val tree = TreeNode(3, left = TreeNode(1, right = TreeNode(2)), right = TreeNode(4))

    val orderedArray = Recursive().toOrderedArray(tree)
    val orderedArray2 = Lc230.Iterative().toOrderedArray(tree)
    println(orderedArray.joinToString())
    println(orderedArray2.joinToString())
}
