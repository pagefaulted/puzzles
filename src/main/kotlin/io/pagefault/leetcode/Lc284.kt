package io.pagefault.leetcode

class PeekingIterator(val iterator:Iterator<Int>):Iterator<Int> {
    private var latest: Int? = if (iterator.hasNext()) iterator.next() else null

    fun peek(): Int = latest!!

    override fun next(): Int = latest!!.also { updateLatest() }

    override fun hasNext(): Boolean = latest != null

    private fun updateLatest() {
        if (iterator.hasNext()) latest = iterator.next() else latest = null
    }
}

/**
 * Your PeekingIterator object will be instantiated and called as such:
 * var obj = PeekingIterator(arr)
 * var param_1 = obj.next()
 * var param_2 = obj.peek()
 * var param_3 = obj.hasNext()
 *
 * Constraints:
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 1000
 * All the calls to next and peek are valid.
 * At most 1000 calls will be made to next, hasNext, and peek.
 */

