package io.pagefault.leetcode

/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.
 */

class Quadratic {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val size = nums.size

        for (i in 0 until size) {
            for (z in i + 1 until size) {
                if (nums[i] + nums[z] == target) {
                    return intArrayOf(i, z)
                }
            }
        }
        return intArrayOf()
    }
}

class Linear {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val size = nums.size
        val value2index = mutableMapOf<Int, Int>()

        for (i in 0 until size) {
            val requiredNumber = target - nums[i]

            if (value2index.containsKey(requiredNumber)) {
                return intArrayOf(i, value2index.get(requiredNumber)!!)
            } else {
                value2index.put(nums[i], i)
            }
        }
        return intArrayOf()
    }
}

fun main() {
    val nums = intArrayOf(3, 2, 3)

    println(Quadratic().twoSum(nums, 6).joinToString())
    println(Linear().twoSum(nums, 6).joinToString())
}