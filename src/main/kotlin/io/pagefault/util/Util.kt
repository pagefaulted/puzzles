package io.pagefault.util

fun <T> time(block: () -> T): T {
    val start = System.nanoTime()
    return block().also {
        println("Exec time ${(System.nanoTime() - start) / (1000 * 1000)}ms")
    }
}
