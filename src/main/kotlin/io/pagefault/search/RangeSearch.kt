package io.pagefault.search

import java.util.*

interface Ids {
    companion object {
        const val NO_IDS_LEFT = -1
    }

    fun next(): Int
}

object EmptyIds : Ids {
    override fun next(): Int = Ids.NO_IDS_LEFT
}

class BitSetIds(private val bitSet: BitSet) : Ids {
    private var currentPosition = 0
    override fun next(): Int = bitSet.nextSetBit(currentPosition++)
}

interface RangeSearch {
    fun findInRange(start: Long, end: Long, startInclusive: Boolean, endInclusive: Boolean): Ids
}

class RangeSearchContainerLinear(private val data: Array<Long>) : RangeSearch {

    override fun findInRange(start: Long, end: Long, startInclusive: Boolean, endInclusive: Boolean): Ids =
        BitSet(data.size).let {
            data.forEachIndexed { idx, value ->
                if (value in start..end) it.set(idx + 1)
            }
            BitSetIds(it)
        }
}

/**
 * NOTE: this class's [compareTo] does not align to [equals].
 */
private class Element(var value: Long, val id: Int) : Comparable<Element> {
    override fun compareTo(other: Element): Int =
        when {
            value < other.value -> -1
            value == other.value -> 0
            else -> 1
        }
}

fun main() {
    val rangeSearch = RangeSearchContainerLinear(arrayOf(5, 3, 6, 8, 2, 1))

    rangeSearch.findInRange(0, 0, true, true)
}
