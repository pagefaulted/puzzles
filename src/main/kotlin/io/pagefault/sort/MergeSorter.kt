package io.pagefault.sort

/**
 * The recursion tree has height lgn, since each step splits the problem in half.
 * Each level of the recursion tree contributes cn (merging),
 * which results in total time complexity cn * lgN + cn, O(nlgn)
 */
object MergeSorter : Sorter {

    override fun sort(elements: List<Long>): List<Long> =
        if (elements.isEmpty()) {
            emptyList()
        } else {
            mergeSort(elements)
        }

    private fun mergeSort(elements: List<Long>): List<Long> {
        if (elements.size == 1) {
            return elements
        }
        val l1Size = elements.size.div(2)
        val l1 = mergeSort(elements.subList(0, l1Size))
        val l2 = mergeSort(elements.subList(l1Size, elements.size))
        return merge(l1, l2)
    }

    private fun merge(l1: List<Long>, l2: List<Long>): List<Long> {
        val mergedList = MutableList(l1.size + l2.size) { 0L }
        val l1Size = l1.size
        val l2Size = l2.size

        var l1Idx = 0
        var l2Idx = 0
        var mergedListIdx = 0

        while (l1Idx < l1Size || l2Idx < l2Size) {
            // Early terminate if one list is already exhausted
            if (l1Idx >= l1Size) {
                l2.subList(l2Idx, l2Size).forEach {
                    mergedList[mergedListIdx++] = it
                }
                break
            } else if (l2Idx >= l2Size) {
                l1.subList(l1Idx, l1Size).forEach {
                    mergedList[mergedListIdx++] = it
                }
                break
            }

            if (l1[l1Idx] < l2[l2Idx]) {
                mergedList[mergedListIdx] = l1[l1Idx++]
            } else {
                mergedList[mergedListIdx] = l2[l2Idx++]
            }
            ++mergedListIdx
        }

        return mergedList
    }
}
