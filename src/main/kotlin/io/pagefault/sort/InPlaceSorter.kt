package io.pagefault.sort

interface InPlaceSorter {

    /**
     * Sort the provided elements in place.
     *
     * @param elements the [List] of elements to be sorted in-place.
     */
    fun sort(elements: MutableList<Long>)
}
