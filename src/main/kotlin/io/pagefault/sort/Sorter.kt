package io.pagefault.sort

interface Sorter {

    /**
     * Return a new [List] containing the elements in the provided [elements] list, in sorted order.
     *
     * @elements the [List] containing the elements to sort.
     * @return a new [List] containing the provided elements in sorted order.
     */
    fun sort(elements: List<Long>): List<Long>
}
