package io.pagefault.sort

object InsertionSorter : InPlaceSorter {

    /**
     * In-place sort, O(n^2).
     */
    override fun sort(elements: MutableList<Long>) {
        for (j in 1 until elements.size) {
            val current = elements[j]
            var prevIdx = j - 1
            while (prevIdx >= 0 && elements[prevIdx] > current) {
                elements[prevIdx + 1] = elements[prevIdx]
                --prevIdx
            }
            elements[prevIdx + 1] = current
        }
    }
}
