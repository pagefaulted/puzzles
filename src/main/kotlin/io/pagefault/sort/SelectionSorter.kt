package io.pagefault.sort

object SelectionSorter: InPlaceSorter {

    /**
     * In-place sort. O(n^2).
     */
    override fun sort(elements: MutableList<Long>) {
        for (i in 0 until elements.size) {
            var smallestIdx = i
            for (j in i + 1 until elements.size) {
                if (elements[j] < elements[smallestIdx]) {
                    smallestIdx = j
                }
            }
            val smallestElem = elements[smallestIdx]
            elements[smallestIdx] = elements[i]
            elements[i] = smallestElem
        }
    }
}
