package io.pagefault.euler

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all the numbers from 1 to 20?
 *
 * Running time is N*K, where K is significant because this becomes a runaway problem for large ranges.
 */
object SmallestMultiple {
    fun smallestMultiple(range: LongRange): Long {
        val originalDividend = range.last
        tailrec fun doIt(dividend: Long, range: LongRange): Long =
            when (isDivisibleByRange(dividend, range)) {
                true -> dividend
                false -> doIt(dividend + originalDividend, range)
            }

        return doIt(range.last, range)
    }

    fun smallestMultipleLoop(range: LongRange): Long {
        var dividend = range.last
        while(true) {
            if (isDivisibleByRange(dividend, range)) {
                return dividend
            } else {
                dividend += range.last
            }
        }
    }

    private fun isDivisibleByRange(dividend: Long, range: LongRange): Boolean = range.all { dividend % it == 0L }
}
