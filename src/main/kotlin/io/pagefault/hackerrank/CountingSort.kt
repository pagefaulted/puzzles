package io.pagefault.hackerrank

fun countingSort(arr: Array<Int>): Array<Int> =
    Array(100) { 0 }.also {
        arr.forEach { v ->
            it[v] = it[v] + 1
        }
    }

fun main(args: Array<String>) {
    val n = readLine()!!.trim().toInt()

    val arr = readLine()!!.trimEnd().split(" ").map{ it.toInt() }.toTypedArray()

    val result = countingSort(arr)

    println(result.joinToString(" "))
}
