package io.pagefault.grok;

import java.util.HashSet;
import java.util.Set;

public class Duplicates {

    public boolean containsDuplicate(int[] nums) {
        final Set<Integer> set = new HashSet<>(nums.length);
        for (int n : nums) {
            if (!set.add(n)) {
                return false;
            }
        }
        return true;
    }
}
