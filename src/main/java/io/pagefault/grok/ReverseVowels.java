package io.pagefault.grok;

public class ReverseVowels {
    public String reverseVowels(String s) {
        if (s == null) {
            return null;
        } else if (s.isEmpty()) {
            return s;
        }

        final char[] string = s.toCharArray();
        int leftIdx = 0;
        int rightIdx = s.length() - 1;

        while (leftIdx < rightIdx) {
            if (!isVowel(string[leftIdx])) {
                ++leftIdx;
                continue;
            }

            if (!isVowel(string[rightIdx])) {
                --rightIdx;
                continue;
            }
            swapChars(string, leftIdx++, rightIdx--);
        }
        return new String(string);
    }

    private static boolean isVowel(final char c) {
        return "aeiouAEIOU".indexOf(c) != -1;
    }

    public static void swapChars(char[] string, int from, int to) {
        char tmp = string[to];
        string[to] = string[from];
        string[from] = tmp;
    }
}
