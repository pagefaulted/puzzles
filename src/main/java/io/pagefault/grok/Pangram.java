package io.pagefault.grok;

public class Pangram {
    private static final int all = (1 << 26) - 1;
    private final int asciiOffset = 97;
    private int lettersBits = 0;

    public boolean checkIfPangram(final String sentence) {
        for (int b : sentence.getBytes()) {
            if (Character.isLetter(b)) {
                lettersBits |= 1 << (Character.toLowerCase(b) - asciiOffset);
            }
        }
        return lettersBits == all;
    }

    public static void main(final String[] args) {
        System.out.println(new Pangram().checkIfPangram("Abcdefghijklmnopqrstuvwxyz"));
        System.out.println(new Pangram().checkIfPangram("abcdefghilmnopqrstuvwxy"));
        System.out.println(new Pangram().checkIfPangram("Abcdefghijklmnopqrstuvwxyz-3415-09-"));
    }
}
