package io.pagefault.grok;

public class Sqrt {

    public int mySqrt(final int x) {
        int candidate = 0;
        int power = 0;

        while ((power = candidate * candidate) < x) {
            ++candidate;
        }

        return power > x ? candidate - 1 : candidate;
    }

    public static void main(final String args[]) {
        System.out.println(new Sqrt().mySqrt(15));
        System.out.println(new Sqrt().mySqrt(8));
        System.out.println(new Sqrt().mySqrt(4));
        System.out.println(new Sqrt().mySqrt(2));
    }
}
